package br.com.LojaAgricola.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Propriedade extends GenericDomain {
	@Column(length = 40, nullable = false)
	private String local;
	@Column(length = 30, nullable = false)
	private String tamanho;
	
	@JoinColumn(nullable = false)
	@ManyToOne
	private Cidade cidade;
	@JoinColumn
	@ManyToOne
	private Agricultor agricultor;
	@JoinColumn
	@ManyToOne
	private Agronomo agronomo;
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	public Agricultor getAgricultor() {
		return agricultor;
	}
	public void setAgricultor(Agricultor agricultor) {
		this.agricultor = agricultor;
	}
	public Agronomo getAgronomo() {
		return agronomo;
	}
	public void setAgronomo(Agronomo agronomo) {
		this.agronomo = agronomo;
	}
}
