package br.com.LojaAgricola.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Agronomo extends GenericDomain {
	@Column(length = 15, nullable = false)
	private String carteira;
	@JoinColumn
	@OneToOne
	private Pessoa pessoa;
	public String getCarteira() {
		return carteira;
	}
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

}
