package br.com.LojaAgricola.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pessoa extends GenericDomain{
	@Column(length = 35, nullable = false)
	private String nome;
	@Column(length = 45, nullable = false)
	private String cpf;
	@Column(length = 45 , nullable = false)
	private String rg;
	@Column(length = 35 , nullable = false)
	private String logradouro;
	@Column(nullable = false)
	private Short numero;
	@Column(length = 25)
	private String bairro;
	@JoinColumn(nullable = false)
	@ManyToOne
	private Cidade cidade;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public Short getNumero() {
		return numero;
	}
	public void setNumero(Short numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	

}
