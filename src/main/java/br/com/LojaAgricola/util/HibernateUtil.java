package br.com.LojaAgricola.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtil {
	
	
	private static SessionFactory criarFabricaDeSessoes() {
		try {
			Configuration configuracao = new Configuration().configure("hibernate.cfg.xml");
			SessionFactory fabrica = configuracao.buildSessionFactory();
			return fabrica;
		} catch (Throwable ex) {
			// TODO: handle exception
			System.err.print("Nao criou a fabrica de sessoes!"+ ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	private static SessionFactory fabricaDeSessoes = criarFabricaDeSessoes();
	
	public static SessionFactory getFabricaDeSessoes() {
		return fabricaDeSessoes;
	}

	

	
}