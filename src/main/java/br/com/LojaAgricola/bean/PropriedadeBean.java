package br.com.LojaAgricola.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.LojaAgricola.dao.AgricultorDAO;
import br.com.LojaAgricola.dao.AgronomoDAO;
import br.com.LojaAgricola.dao.CidadeDAO;
import br.com.LojaAgricola.dao.EstadoDAO;
import br.com.LojaAgricola.dao.PropriedadeDAO;
import br.com.LojaAgricola.domain.Agricultor;
import br.com.LojaAgricola.domain.Agronomo;
import br.com.LojaAgricola.domain.Cidade;
import br.com.LojaAgricola.domain.Estado;
import br.com.LojaAgricola.domain.Propriedade;

@ViewScoped
@ManagedBean(name = "propriedadeBean")
public class PropriedadeBean implements Serializable {
	private Propriedade propriedade;
	private ArrayList<Propriedade> propriedades;
	private Estado estado;
	private Cidade cidade;

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	private ArrayList<Agronomo> agronomos;
	private ArrayList<Agricultor> agricultores;

	private ArrayList<Estado> estados;
	private ArrayList<Cidade> cidades;

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public ArrayList<Propriedade> getPropriedades() {
		return propriedades;
	}

	public void setPropriedades(ArrayList<Propriedade> propriedades) {
		this.propriedades = propriedades;
	}

	public ArrayList<Agronomo> getAgronomos() {
		return agronomos;
	}

	public void setAgronomos(ArrayList<Agronomo> agronomos) {
		this.agronomos = agronomos;
	}

	public ArrayList<Agricultor> getAgricultores() {
		return agricultores;
	}

	public void setAgricultores(ArrayList<Agricultor> agricultores) {
		this.agricultores = agricultores;
	}

	public ArrayList<Estado> getEstados() {
		return estados;
	}

	public void setEstados(ArrayList<Estado> estados) {
		this.estados = estados;
	}

	public ArrayList<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(ArrayList<Cidade> cidades) {
		this.cidades = cidades;
	}

	@PostConstruct
	public void listar() {
		try {
			PropriedadeDAO propriedadeDAO = new PropriedadeDAO();
			propriedades = propriedadeDAO.listar();
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("não foi possivel listar");
			e.printStackTrace();
		}
	}

	public void novo() {
		try {
			propriedade = new Propriedade();
			estado = new Estado();
			cidade = new Cidade();
			AgronomoDAO agronomoDAO = new AgronomoDAO();
			agronomos = agronomoDAO.listar();
			AgricultorDAO agricultorDAO = new AgricultorDAO();
			agricultores = agricultorDAO.listar();
			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar();
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
			Messages.addGlobalInfo("salvo com sucesso");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel CRIAR NOVO!");
		}
	}

	public void salvar() {
		try {
		PropriedadeDAO propriedadeDAO = new PropriedadeDAO();
		propriedadeDAO.salvar(propriedade);
			
		propriedade = new Propriedade();
		estado = new Estado();
		
		AgronomoDAO agronomoDAO = new AgronomoDAO();
		agronomos = agronomoDAO.listar();
		AgricultorDAO agricultorDAO = new AgricultorDAO();
		agricultores = agricultorDAO.listar();
		EstadoDAO estadoDAO = new EstadoDAO();
		estados = estadoDAO.listar();
		CidadeDAO cidadeDAO = new CidadeDAO();
		cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
		Messages.addGlobalInfo("salvo com sucesso");
	} catch (RuntimeException e) {
		// TODO: handle exception
		Messages.addGlobalError("nao foi possivel CRIAR NOVO!");
	}	
}
	public void editar(ActionEvent evento) {
		try {
			propriedade = (Propriedade) evento.getComponent().getAttributes().get("propriedadeselecionada");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("não foi poossivel editar");
			e.printStackTrace();
		}
		
	}
	public void excluir(ActionEvent evento) {
		try {
			propriedade = (Propriedade) evento.getComponent().getAttributes().get("propriedadeselecionada");
			PropriedadeDAO propriedadeDAO = new PropriedadeDAO();
			propriedadeDAO.excluir(propriedade);
			propriedades = propriedadeDAO.listar();
			Messages.addGlobalInfo("Propriedade Excluida!");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("NÃO foi possivel EXCLUIR");
			e.printStackTrace();
		}
		
	}
	public void popular() {
		try {
			if (estado != null) {
				CidadeDAO cidadeDAO = new CidadeDAO();
				cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
			} else {
				cidades = new ArrayList<>();
			}
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar filtrar as cidades");
			erro.printStackTrace();
		}
	}
}
