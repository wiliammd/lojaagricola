package br.com.LojaAgricola.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.LojaAgricola.dao.AgricultorDAO;
import br.com.LojaAgricola.dao.PessoaDAO;
import br.com.LojaAgricola.domain.Agricultor;
import br.com.LojaAgricola.domain.Pessoa;
@ViewScoped
@ManagedBean(name = "agricultorBean")
public class AgricultorBean implements Serializable {

	private Agricultor agricultor;
	private Pessoa pessoa;
	private ArrayList<Agricultor> agricultores;
	private ArrayList<Pessoa> pessoas;
	

	
	public Agricultor getAgricultor() {
		return agricultor;
	}
	public void setAgricultor(Agricultor agricultor) {
		this.agricultor = agricultor;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public ArrayList<Agricultor> getAgricultores() {
		return agricultores;
	}
	public void setAgricultores(ArrayList<Agricultor> agricultores) {
		this.agricultores = agricultores;
	}
	public ArrayList<Pessoa> getPessoas() {
		return pessoas;
	}
	public void setPessoas(ArrayList<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
	@PostConstruct
	public void listar() {
		try {
			AgricultorDAO agricultorDAO = new AgricultorDAO();
			agricultores = agricultorDAO.listar();
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("não foi possivel listar agricultores!");
			e.printStackTrace();
		}
	}
	public void salvar() {
		try {
			AgricultorDAO agricultorDAO = new AgricultorDAO();
			agricultorDAO.salvar(agricultor);
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
			agricultores = agricultorDAO.listar();
			Messages.addGlobalInfo("Pessoa salva com sucesso!");
		} catch (RuntimeException e) {
			// TODO: handle exception
		}
	}
	public void novo() {
		try {
			agricultor = new Agricultor();
			pessoa = new Pessoa();
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
			
		} catch (Exception e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel criar novo agricultor");
			e.printStackTrace();
		}
	}
	
	
	public void excluir(ActionEvent evento) {
		try {
			agricultor = (Agricultor) evento.getComponent().getAttributes().get("agricultorselecionado");
			AgricultorDAO agricultorDAO = new AgricultorDAO();
			agricultorDAO.excluir(agricultor);
			agricultores = agricultorDAO.listar();
			Messages.addGlobalInfo("Excluido com sucesso!");
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel EXCLUIR");
			e.printStackTrace();
		}
	}
	public void editar(ActionEvent evento) {
		try {
			agricultor = (Agricultor) evento.getComponent().getAttributes().get("agricultorselecionado");
//			AgricultorDAO agricultorDAO = new AgricultorDAO();
//			agricultorDAO.editar(agricultor);
//			agricultores = agricultorDAO.listar();
			Messages.addGlobalInfo("Excluido com sucesso!");
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel EXCLUIR");
			e.printStackTrace();
		}
	}
}
