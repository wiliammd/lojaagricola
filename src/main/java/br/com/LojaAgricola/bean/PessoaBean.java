package br.com.LojaAgricola.bean;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.LojaAgricola.dao.CidadeDAO;
import br.com.LojaAgricola.dao.EstadoDAO;
import br.com.LojaAgricola.dao.PessoaDAO;
import br.com.LojaAgricola.domain.Cidade;
import br.com.LojaAgricola.domain.Estado;
import br.com.LojaAgricola.domain.Pessoa;

@ViewScoped
@ManagedBean(name = "pessoaBean")
public class PessoaBean {
	private Pessoa pessoa;
	private ArrayList<Pessoa> pessoas;
	private Estado estado;
	
	private ArrayList<Estado> estados;
	private ArrayList<Cidade> cidades;
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public ArrayList<Pessoa> getPessoas() {
		return pessoas;
	}
	public void setPessoas(ArrayList<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public ArrayList<Estado> getEstados() {
		return estados;
	}
	public void setEstados(ArrayList<Estado> estados) {
		this.estados = estados;
	}
	public ArrayList<Cidade> getCidades() {
		return cidades;
	}
	public void setCidades(ArrayList<Cidade> cidades) {
		this.cidades = cidades;
	}
	@PostConstruct
	public void Listar() {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("Erro ao listar Pessoas!");
			e.printStackTrace();
		}
	}
	public void novo() {
		try {
			pessoa = new Pessoa();
			estado = new Estado();

			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar();
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("erro ao CRIAR NOVA pessoa!");
		}
	}
	public void editar(ActionEvent evento) {
		try {
			pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaselecionada");
//			PessoaDAO pessoaDAO = new PessoaDAO();
//			pessoaDAO.listar();
//			EstadoDAO estadoDAO = new EstadoDAO();
//			estadoDAO.listar();
//			CidadeDAO cidadeDAO = new CidadeDAO();
//			cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
//			
		} catch (RuntimeException e) {
			// TODO: handle exception
		}
		
	}
	public void salvar() {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.salvar(pessoa);
			
			pessoas = pessoaDAO.listar();
			
			pessoa = new Pessoa();
			
			estado = new Estado();

			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar();
			CidadeDAO cidadeDAO = new CidadeDAO();

			cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
			Messages.addGlobalInfo("Salvando pessoa");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar a pessoa");
			erro.printStackTrace();
		}
	}
	public void excluir(ActionEvent evento) {
		try {
		pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaselecionada");
		PessoaDAO pessoaDAO = new PessoaDAO();
		pessoaDAO.excluir(pessoa);
		pessoas = pessoaDAO.listar();
		Messages.addGlobalInfo("Excluindo pessoa");
		}catch(RuntimeException erro) {
			Messages.addGlobalError("Não foi possivel EXCLUIR pessoa!");
			erro.printStackTrace();
		}
	}
	
	public void popular() {
		try {
			if (estado != null) {
				CidadeDAO cidadeDAO = new CidadeDAO();
				cidades = cidadeDAO.buscarPorEstado(estado.getCodigo());
			} else {
				cidades = new ArrayList<>();
			}
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar filtrar as cidades");
			erro.printStackTrace();
		}
	}

}
