package br.com.LojaAgricola.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.LojaAgricola.dao.AgronomoDAO;
import br.com.LojaAgricola.dao.PessoaDAO;
import br.com.LojaAgricola.domain.Agronomo;
import br.com.LojaAgricola.domain.Pessoa;

@ViewScoped
@ManagedBean(name = "agronomoBean")
public class AgronomoBean implements Serializable {

	private Agronomo agronomo;
	private Pessoa pessoa;
	private ArrayList<Agronomo> agronomos;
	private ArrayList<Pessoa> pessoas;
	

	
	public Agronomo getAgronomo() {
		return agronomo;
	}
	public void setAgronomo(Agronomo agronomo) {
		this.agronomo = agronomo;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public ArrayList<Agronomo> getAgronomoes() {
		return agronomos;
	}
	public void setAgronomoes(ArrayList<Agronomo> agronomos) {
		this.agronomos = agronomos;
	}
	public ArrayList<Pessoa> getPessoas() {
		return pessoas;
	}
	public void setPessoas(ArrayList<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
	@PostConstruct
	public void listar() {
		try {
			AgronomoDAO agronomoDAO = new AgronomoDAO();
			agronomos = agronomoDAO.listar();
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("não foi possivel listar agronomos!");
			e.printStackTrace();
		}
	}
	public void salvar() {
		try {
			AgronomoDAO agronomoDAO = new AgronomoDAO();
			agronomoDAO.salvar(agronomo);
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
			agronomos = agronomoDAO.listar();
			Messages.addGlobalInfo("Pessoa salva com sucesso!");
		} catch (RuntimeException e) {
			// TODO: handle exception
		}
	}
	public void novo() {
		try {
			agronomo = new Agronomo();
			pessoa = new Pessoa();
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
			
		} catch (Exception e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel criar novo agronomo");
			e.printStackTrace();
		}
	}
	
	
	public void excluir(ActionEvent evento) {
		try {
			agronomo = (Agronomo) evento.getComponent().getAttributes().get("agronomoselecionado");
			AgronomoDAO agronomoDAO = new AgronomoDAO();
			agronomoDAO.excluir(agronomo);
			agronomos = agronomoDAO.listar();
			Messages.addGlobalInfo("Excluido com sucesso!");
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel EXCLUIR");
			e.printStackTrace();
		}
	}
	public void editar(ActionEvent evento) {
		try {
			agronomo = (Agronomo) evento.getComponent().getAttributes().get("agronomoselecionado");
//			AgronomoDAO agronomoDAO = new AgronomoDAO();
//			agronomoDAO.editar(agronomo);
//			agronomos = agronomoDAO.listar();
			Messages.addGlobalInfo("Excluido com sucesso!");
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("nao foi possivel EXCLUIR");
			e.printStackTrace();
		}
	}
}
