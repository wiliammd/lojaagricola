package br.com.LojaAgricola.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.LojaAgricola.dao.CidadeDAO;
import br.com.LojaAgricola.dao.EstadoDAO;
import br.com.LojaAgricola.domain.Cidade;
import br.com.LojaAgricola.domain.Estado;

@ViewScoped
@ManagedBean(name ="cidadeBean")
public class CidadeBean implements Serializable {
	private Cidade cidade;
	private ArrayList<Cidade> cidades;
	private ArrayList<Estado> estados;
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public ArrayList<Cidade> getCidades() {
		return cidades;
	}
	public void setCidades(ArrayList<Cidade> cidades) {
		this.cidades = cidades;
	}
	public ArrayList<Estado> getEstados() {
		return estados;
	}
	public void setEstados(ArrayList<Estado> estados) {
		this.estados = estados;
	}
	@PostConstruct
	public void listar() {
		try {
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidades = cidadeDAO.listar();
			
		} catch (RuntimeException erro) {
			// TODO: handle exception
			Messages.addGlobalError("Não foi possivel listar cidades");
			erro.printStackTrace();
		}
	}
	public void novo() {
		try {
			cidade = new Cidade();
			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar();
			
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("Não foi possivel criar nova cidade");
			e.printStackTrace();
		}
	}
	public void salvar() {
		try {
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidadeDAO.salvar(cidade);
			
			cidade = new Cidade();
			EstadoDAO estadoDAO = new EstadoDAO();
			cidades = cidadeDAO.listar();
			estados = estadoDAO.listar();
			Messages.addGlobalInfo("Cidade salva com sucesso!");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("Erro ao salvar ciadade");
			e.printStackTrace();
		}
	}
	public void editar(ActionEvent evento) {
		try {
			cidade = (Cidade) evento.getComponent().getAttributes().get("cidadeselecionada");
			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar();
			Messages.addGlobalInfo("Cidade ALTERADA com Sucesso!");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("Não foi possivel alterar cidade");
			e.getStackTrace();
		}
	}
	public void excluir(ActionEvent evento) {
		try {
			cidade = (Cidade) evento.getComponent().getAttributes().get("cidadeselecionada");
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidadeDAO.excluir(cidade);
			cidades = cidadeDAO.listar();
			Messages.addGlobalInfo("Cidade Excluida com sucesso!");
		} catch (RuntimeException e) {
			// TODO: handle exception
			Messages.addGlobalError("erro ao EXCLUIR cidade");
			e.printStackTrace();
		}
	}

}
